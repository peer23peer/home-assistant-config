import logging
import math
from datetime import timedelta

import RPi.GPIO as GPIO
from homeassistant.const import TEMP_CELSIUS
from homeassistant.helpers.entity import Entity
from homeassistant.util import Throttle
from pi_sht1x import SHT1x

DATA_PIN = 18
SCK_PIN = 23

_LOGGER = logging.getLogger(__name__)

MIN_TIME_BETWEEN_UPDATES = timedelta(seconds=30)


def setup_platform(hass, config, add_devices, discovery_info=None):
    """Setup the sensor platform."""
    add_devices([SHT1xTemperatureSensor(),
                 SHT1xHumiditySensor()])


class SHT1xHumiditySensor(Entity):
    """Representation of a Sensor."""

    def __init__(self):
        """Initialize the sensor."""
        self._state = None

    @property
    def name(self):
        """Return the name of the sensor."""
        return 'SHT1x Humidity'

    @property
    def state(self):
        """Return the state of the sensor."""
        return self._state

    @property
    def unit_of_measurement(self):
        """Return the unit of measurement."""
        return '%'

    @Throttle(MIN_TIME_BETWEEN_UPDATES)
    def update(self):
        """Fetch new state data for the sensor.

        This is the only method that should fetch new data for Home Assistant.
        """
        with SHT1x(DATA_PIN, SCK_PIN, gpio_mode=GPIO.BCM, vdd='3.5V', resolution='High',
                   heater=False, otp_no_reload=False, crc_check=True) as sensor:
            temp = sensor.read_humidity()
        if math.isnan(temp):
            _LOGGER.warning("Bad humidity sample from sensor SHT1x")
        self._state = temp


class SHT1xTemperatureSensor(Entity):
    """Representation of a Sensor."""

    def __init__(self):
        """Initialize the sensor."""
        self._state = None

    @property
    def name(self):
        """Return the name of the sensor."""
        return 'SHT1x Temperature'

    @property
    def state(self):
        """Return the state of the sensor."""
        return self._state

    @property
    def unit_of_measurement(self):
        """Return the unit of measurement."""
        return TEMP_CELSIUS

    @Throttle(MIN_TIME_BETWEEN_UPDATES)
    def update(self):
        """Fetch new state data for the sensor.

        This is the only method that should fetch new data for Home Assistant.
        """
        with SHT1x(DATA_PIN, SCK_PIN, gpio_mode=GPIO.BCM, vdd='3.5V', resolution='High',
                   heater=False, otp_no_reload=False, crc_check=True) as sensor:
            temp = sensor.read_temperature()
        if math.isnan(temp):
            _LOGGER.warning("Bad temperature sample from sensor SHT1x")
        self._state = temp

