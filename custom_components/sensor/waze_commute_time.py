import logging
from datetime import time, datetime

import homeassistant.helpers.config_validation as cv
import voluptuous as vol
from homeassistant.components.sensor.waze_travel_time import \
    PLATFORM_SCHEMA, WazeTravelTime, CONF_DESTINATION, CONF_ORIGIN, \
    CONF_REGION, CONF_INCL_FILTER, CONF_EXCL_FILTER, CONF_REALTIME
from homeassistant.const import (CONF_NAME, EVENT_HOMEASSISTANT_START)

_LOGGER = logging.getLogger(__name__)

ATTR_COMMUTE = 'commute'

CONF_MORNING_START_HOUR = 'morning_start_hour'
CONF_MORNING_END_HOUR = 'morning_end_hour'
CONF_AFTERNOON_START_HOUR = 'afternoon_start_hour'
CONF_AFTERNOON_END_HOUR = 'afternoon_end_hour'

DEFAULT_NAME = 'Waze Commute Time'

PLATFORM_SCHEMA = PLATFORM_SCHEMA.extend({
    vol.Optional(CONF_NAME, default=DEFAULT_NAME): cv.string,
    vol.Optional(CONF_MORNING_START_HOUR): cv.string,
    vol.Optional(CONF_MORNING_END_HOUR): cv.string,
    vol.Optional(CONF_AFTERNOON_START_HOUR): cv.string,
    vol.Optional(CONF_AFTERNOON_END_HOUR): cv.string,
})


def setup_platform(hass, config, add_entities, discovery_info=None):
    """Set up the Waze travel time sensor platform."""
    destination = config.get(CONF_DESTINATION)
    name = config.get(CONF_NAME)
    origin = config.get(CONF_ORIGIN)
    region = config.get(CONF_REGION)
    incl_filter = config.get(CONF_INCL_FILTER)
    excl_filter = config.get(CONF_EXCL_FILTER)
    realtime = config.get(CONF_REALTIME)
    morning_start_hour = config.get(CONF_MORNING_START_HOUR)
    morning_end_hour = config.get(CONF_MORNING_END_HOUR)
    afternoon_start_hour = config.get(CONF_AFTERNOON_START_HOUR)
    afternoon_end_hour = config.get(CONF_AFTERNOON_END_HOUR)

    sensor = WazeCommuteTime(name, origin, destination, region,
                             incl_filter, excl_filter, realtime,
                             morning_start_hour, morning_end_hour,
                             afternoon_start_hour, afternoon_end_hour)

    add_entities([sensor])

    # Wait until start event is sent to load this component.
    hass.bus.listen_once(
        EVENT_HOMEASSISTANT_START, lambda _: sensor.update())


class WazeCommuteTime(WazeTravelTime):

    def __init__(self, name, origin, destination, region,
                 incl_filter, excl_filter, realtime,
                 morning_start_hour, morning_end_hour,
                 afternoon_start_hour, afternoon_end_hour):
        super(WazeCommuteTime, self).__init__(name, origin, destination, region,
                                              incl_filter, excl_filter, realtime)
        self._morning_start_hour = time(hour=int(morning_start_hour))
        self._morning_end_hour = time(hour=int(morning_end_hour))
        self._afternoon_start_hour = time(hour=int(afternoon_start_hour))
        self._afternoon_end_hour = time(hour=int(afternoon_end_hour))
        self._morning_origin_entity_id = self._origin_entity_id
        self._morning_destination_entity_id = self._destination_entity_id
        self._afternoon_origin_entity_id = self._destination_entity_id
        self._afternoon_destination_entity_id = self._origin_entity_id
        if self._origin_entity_id is None:
            self._morning_origin = self._origin
            self._afternoon_destination = self._origin
        if self._destination_entity_id is None:
            self._morning_destination = self._destination
            self._afternoon_origin = self._destination

    def update(self):
        if self._morning_start_hour.hour <= datetime.now().hour <= self._morning_end_hour.hour:
            self._origin = self._morning_origin
            self._destination = self._morning_destination
            super(WazeCommuteTime, self).update()
            self._state['commute'] = 'to work'
        elif self._afternoon_start_hour.hour <= datetime.now().hour <= self._afternoon_end_hour.hour:
            self._origin = self._afternoon_origin
            self._destination = self._afternoon_destination
            super(WazeCommuteTime, self).update()
            self._state['commute'] = 'to home'

    @property
    def device_state_attributes(self):
        res = super(WazeCommuteTime, self).device_state_attributes()
        if 'commute' in self._state:
            res[ATTR_COMMUTE] = self._state['commute']
        return res
