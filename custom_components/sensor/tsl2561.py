import logging
import math
from datetime import timedelta

import board
import busio
import adafruit_tsl2561

from homeassistant.helpers.entity import Entity
from homeassistant.util import Throttle

_LOGGER = logging.getLogger(__name__)

MIN_TIME_BETWEEN_UPDATES = timedelta(seconds=30)


def setup_platform(hass, config, add_devices, discovery_info=None):
    """Setup the sensor platform."""
    add_devices([TSL2561LuxSensor()])


class TSL2561LuxSensor(Entity):
    """Representation of a Sensor."""

    def __init__(self):
        """Initialize the sensor."""
        self._state = None
        self._i2c = busio.I2C(board.SCL, board.SDA)
        self._sensor = adafruit_tsl2561.TSL2561(self._i2c)

    @property
    def name(self):
        """Return the name of the sensor."""
        return 'TSL2561 Lux sensor'

    @property
    def state(self):
        """Return the state of the sensor."""
        return self._state

    @property
    def unit_of_measurement(self):
        """Return the unit of measurement."""
        return 'Lux'

    @Throttle(MIN_TIME_BETWEEN_UPDATES)
    def update(self):
        """Fetch new state data for the sensor.

        This is the only method that should fetch new data for Home Assistant.
        """
        lux_value = self._sensor.lux
        if lux_value is None:
            _LOGGER.warning("Lux value is None. Possible sensor underrange or overrange.")
            lux_value = 0.
        self._state = round(lux_value * 1000)/1000

